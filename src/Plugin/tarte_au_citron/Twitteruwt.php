<?php

namespace Drupal\tarte_au_citron_twitteruwt\Plugin\tarte_au_citron;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tarte_au_citron\ServicePluginBase;

/**
 * A Twitteruwt service plugin for "Tarte au citron".
 *
 * @TarteAuCitronService(
 *   id = "twitteruwt",
 *   title = @Translation("Twitter Universal Website Tag")
 * )
 */
class Twitteruwt extends ServicePluginBase {

  /**
   * Account key field.
   *
   * @const string
   */
  public const FIELD_TWITTER_KEY = 'twitteruwtId';


  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        self::FIELD_TWITTER_KEY => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings_form = parent::settingsForm($form, $form_state);

    $settings_form[self::FIELD_TWITTER_KEY] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter uwt id'),
      '#default_value' => $this->getSetting(self::FIELD_TWITTER_KEY),
      '#required' => TRUE,
    ];

    return $settings_form;
  }

}
